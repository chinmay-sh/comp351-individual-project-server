# COMP 351 server

A Simple server built using NodeJS for comp 351 project.

### Links:

* https://ipserver.chinmays.me/

* https://individual-project-server-wkh4p.ondigitalocean.app/

## Built With

* NodeJS
* ExpressJS
* Mysql

## Authors

* **Chinmay Sharma**
