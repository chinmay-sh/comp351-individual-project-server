// const { request, response } = require('express');
const db = require('./config');

function createTables() {
    const createTableQuery = [
        'CREATE TABLE IF NOT EXISTS questions (id INT AUTO_INCREMENT PRIMARY KEY, questionBody VARCHAR(255), correctOpt INT)',
        'CREATE TABLE IF NOT EXISTS options (optid INT AUTO_INCREMENT PRIMARY KEY, quesid INT, optvalue VARCHAR(255), optionNum INT)'
    ]

    createTableQuery.forEach(query => db.query(query, (err, result) => {
        if (err) throw err;
        console.log('Table created');
    }));
}


// add a question
const addQuestion = (request, response) => {
    let { quesBody, correctOption, optionList } = request.body;
    db.query(`INSERT INTO questions (questionBody, correctOpt) VALUES ('${quesBody}', '${correctOption}');`, (error, result, fields) => {
        if (error) {
            response.status(400).send('ERROR 400: Bad Request!')
            throw error
        }
        // console.log(result.insertId)
        optionList.forEach((option, index) => db.query(`INSERT INTO options (quesid, optvalue, optionNum) VALUES ('${result.insertId}','${option}','${index}')`, (error, res, field) => {
            if (error) {
                // response.status(400).send('ERROR 400: Bad Request!')
                throw error
            }
            console.log('SUCCESS Options added')
        }))

        response.status(201).send('SUCCESS 201: Question added')
    })

};

// update a question
const updateQuestion = (request, response) => {
    let quesID = (request.params.id);
    let { quesBody, correctOption, optionList } = request.body;
    db.query('UPDATE questions SET questionBody = ?, correctOpt = ? WHERE id = ?', [quesBody, correctOption, quesID], (error, results) => {
        if (error) {
            response.status(400).send('ERROR 400: Bad Request!')
            throw error
        }
        db.query('DELETE FROM options WHERE quesid = ?',[quesID],(error, res)=> {
            if (error) {
                throw error
            }
            console.log("Previous options Deleted");

            optionList.forEach((option, index) => db.query(`SELECT * from options where quesid = ${quesID} and optionNum = ${index}`, (error, res, field) => {
                if (error) {
                    throw error
                }
                if(res.length == 0){
                    db.query(`INSERT INTO options (quesid, optvalue, optionNum) VALUES (?, ?, ?)`,[quesID,option,index],(error, res, field)=>{
                        if (error) {
                            throw error
                        }
                        console.log("Update Option Inserted")
                    });
                } else {
                    db.query('UPDATE options SET optvalue = ? WHERE quesid = ? AND optionNum = ?',[option,quesID,index], (error, res, field) => {
                        if (error) {
                            throw error
                        }
                        console.log('SUCCESS Options Updated')
                    });
                }
            }));

            response.status(200).send('SUCCESS 200: Question updated')
        });
        
        
    })
};


// get all questions
const getQuestions = (request, response) => {
    db.getConnection(function (err, connection) {
        if (err) throw err; // not connected!

        let result = []
        // Use the connection
        connection.query('SELECT id,questionBody,correctOpt FROM questions ORDER BY questions.id ASC').on('error', function (err) {
            // Handle error, an 'end' event will be emitted after this as well
            if (error) throw error;
        })
            .on('result', function (row) {
                // Pausing the connnection is useful if your processing involves I/O
                connection.pause();
                db.query(`SELECT optvalue,optionNum FROM options WHERE quesid = ${row.id}`,function(err,res){
                    if(err) throw err;

                    let oplist = res;

                    oplist.sort((a, b) => (a.optionNum > b.optionNum) ? 1 : -1)

                    result.push({
                        "id":row.id,
                        "questionBody": row.questionBody,
                        "correctOpt": row.correctOpt,
                        "options": oplist
                    });

                    connection.resume()
                });
                
            })
            .on('end', function () {
                // all rows have been received
                connection.release()
                response.status(201).json(result)
            });;
    });

};

const deleteQuestion = (request, response) => {
    let quesID = (request.params.id);
    db.query('DELETE FROM questions WHERE id = ?', [quesID], (error, results) => {
        if (error) {
            throw error
        }
        db.query('DELETE FROM options WHERE quesid = ?',[quesID],(error, res)=> {
            if (error) {
                throw error
            }
            console.log("Question Deleted")
        });
        response.status(200).send('SUCCESS 200: Question Deleted')
        });
        
}

module.exports = {
    getQuestions,
    addQuestion,
    updateQuestion,
    createTables,
    deleteQuestion
}