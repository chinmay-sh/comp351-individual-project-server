const express = require('express')
const bodyParser = require('body-parser')
const queries = require('./queries')
var cors = require('cors')
const app = express()

app.use(cors())

app.use(bodyParser.json())
app.use(
  bodyParser.urlencoded({
    extended: true,
  })
)

queries.createTables()

//Index Route
app.get('/', (request, response) => {
    response.send('<title>COMP351</title> <h1>WELCOME TO COMP351 PROJECT</h1>')
  });

// Get all questions
app.get('/questions',queries.getQuestions);

// Add a question
app.post('/questions',queries.addQuestion);

// Update a question
app.put('/questions/:id',queries.updateQuestion);

// Delete a question
app.delete('/questions/:id',queries.deleteQuestion);

// Run Server
let port = process.env.PORT;
if (port == null || port == "") {
  port = 8000;
}

app.listen(port, () => {
    console.log(`App running on port ${port}.`)
  });